package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	var participants int
	var salary int
	fmt.Print("How many people in the meeting: ")
	_, err := fmt.Scanf("%d", &participants)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Print("What's the average salary of the people in the room (hourly): ")
	_, err = fmt.Scanf("%d", &salary)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	dollarsPerSecond := (float32)(participants*salary) / 3600
	var spentSoFar float32
	for {
		time.Sleep(1000000000)
		spentSoFar = spentSoFar + dollarsPerSecond
		fmt.Printf("So far this meeting has cost: %v\n", spentSoFar)
	}
}
